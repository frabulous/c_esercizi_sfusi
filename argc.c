/*
Esempio di programma il cui main() riceve argomenti da linea di comando.
I due argomenti saranno file di testo ed il main deve copiare il primo nel secondo.
Eseguire con:
./main.o input.txt output.txt
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char* argv[]){

	FILE* f_in;
	FILE* f_out;

	if(argc != 3){
		printf("Errore! Sono richiesti 2 argomenti:\n nome file di input e nome file di output.\nSono stati forniti %d argomenti.\n", argc-1);
		exit(1);	
	}

	f_in = fopen(argv[1], "r");
	f_out = fopen(argv[2], "w");

	if (f_in==NULL || f_out==NULL){
		printf("Errore! I file non sono stati aperti correttamente.\n");
		exit(2);
	}

	char line[100];
	while(fgets(line,100,f_in) != NULL){
		fprintf(f_out, "%s", line);
	}
	fclose(f_in);
	fclose(f_out);

	return 0;
}
