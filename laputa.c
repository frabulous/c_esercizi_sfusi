/*
Viene passato un file al main contenente una matrice DIMY*DIMX di valori interi >=0
Restituire i punti nei quali si ha il valore più alto sia nella riga che nella relativa colonna
(inclusi i punti dove i valori max su riga o colonna sono uguali al massimo)
*/

#include <stdio.h>
#include <stdlib.h>

#define DIMX 8
#define DIMY 5

int main(int argc, char const *argv[])
{
	FILE* f_in;
	int mat[DIMY][DIMX];
	int max_array[DIMY];
	for (int i = 0; i < DIMY; ++i)
	{
		max_array[i] = 0;
	}

	//controllo input
	if (argc != 2)
	{
		printf("Deve essere fornito 1 parametro di tipo file testuale alla funzione.\nSono stati forniti %d argomenti", argc-1);
		exit(1);
	}

	//apertura file
	f_in = fopen(argv[1], "r");

	if (f_in==NULL)
	{
		printf("Il file non è stato aperto correttamente.\n");
		exit(2);
	}

	//lettura file
	for (int i = 0; i < DIMY; i++)
	{
		for (int j = 0; j < DIMX; j++)
		{
			fscanf(f_in, "%d", &mat[i][j]);
			//sfrutto questa scansione per salvare il valore massimo di ogni riga
			if (mat[i][j] > max_array[i])
			{
				max_array[i] = mat[i][j];
			}
		}	
	}
	fclose(f_in);

	//calcolo risultato

	for (int i = 0; i < DIMY; ++i)
	{
		for (int j = 0; j < DIMX; j++)
		{
			int tmp = mat[i][j];
			if (tmp == max_array[i] && tmp>0)
			{
				int tmp_result = 1;
				for (int k=0; k < DIMY; k++)
				{
					int tmp2 = mat[k][j];
					if (tmp2 > tmp)
					{
						//il punto NON è il più alto nella colonna
						tmp_result = 0;
						break;
					}
				}
				if (tmp_result)
				{
					//il punto va bene
					printf("Possibile collocazione del palazzo: (i,j) = (%d,%d)\n", i, j);
				}
			}
		}
	}

	return 0;
}
