/*
Liste concatenate in C
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct S_item {
	int value; 
} Item;

typedef struct S_node {
	Item info; //informazione contenuta nel nodo
	struct S_node* next; //puntatore al nodo successivo
} Node;

typedef struct S_list {
	Node* head; //puntatore al primo nodo della lista
	Node* tail; //puntatore all'ultimo nodo della lista (utile per liste doppiamente concatenate)
	int size; //numero di elementi contenuti nella lista
} List;

List createList();
void insertAtTail(List* p_list, Item* item);
void printItem(Item item);
void printList(List* list);


int main(int argc, char const *argv[])
{
	List my_list;

	my_list = createList();
	printf("%s\n", "Creata!");


	Item elem_0, elem_1, elem_2;
	elem_0.value = 3;
	elem_1.value = 6;
	elem_2.value = 9;

	insertAtTail(&my_list, &elem_0);
	printf("Size = %d\n", my_list.size);
	insertAtTail(&my_list, &elem_1);
	printf("Size = %d\n", my_list.size);
	insertAtTail(&my_list, &elem_2);
	printf("Size = %d\n", my_list.size);

	printList(&my_list);

	return 0;
}

List createList(){
	List new_list;
	new_list.size = 0;
	new_list.head = NULL;
	new_list.tail = NULL;

	return new_list;
}

void insertAtTail(List* p_list, Item* item){
	
	Node* new_node;
	new_node = (Node*)malloc(sizeof(Node));
	if (new_node==NULL)
	{
		printf("Errore di allocazione!\n");
		exit(1);
	}

	new_node->info = *item;
	new_node->next = NULL;

	if (p_list->size==0)
	{
		p_list->head = new_node;
		p_list->tail = new_node;
		printf("%s\n", "Zero!");
	}
	else
	{
		(p_list->tail)->next = new_node;
		p_list->tail = new_node;
	}
	free(new_node);

	p_list->size+=1;
	printf("Aggiunto! Now size is %d\n", p_list->size);
}

void printItem(Item item){
	printf("Item: %d\n", item.value);
}

void printList(List* p_list){

	Node* current = p_list->head;

	while(current!=NULL)
	{
		printItem(current->info); // TODO: some error in item
		current = current->next;
	}
}