#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct S_node
{
	int value;
	struct S_node* next;
	struct S_node* prev;
} Node;

typedef struct S_list
{
	int size;
	Node* head;
	Node* tail;
} List;

//---FUNCS PROTOTYPES---
List getNewList();
Node* getNewNode();
void insertAtHead(List* p_list, int v);
void insertAtTail(List* p_list, int v);
int removeAtHead(List* p_list);
int removeAtTail(List* p_list);
int searchInList(List* p_list, int v);
void printList(List* p_list);

//---MAIN---
int main(int argc, char const *argv[])
{
	List my_list = getNewList();

	insertAtHead(&my_list, 12);
	insertAtHead(&my_list, 8);
	insertAtHead(&my_list, 4);
	insertAtTail(&my_list, 16);

	printList(&my_list);

	int i = removeAtHead(&my_list);
	printf("Removed %d from head.\n", i);
	printList(&my_list);
	i = removeAtTail(&my_list);
	printf("Removed %d from tail.\n", i);
	printList(&my_list);

	int element;
	printf("Insert the element to look for:\n");
	scanf("%d", &element);
	int r = searchInList(&my_list, element);
	if(r<0) printf("Element is not in list.\n");
	else printf("There is at least one element that matches, position %d.\n", r);

	return 0;
}

//---FUNCS IMPLEMENTATION---
List getNewList(){
	List* p_result = (List*)malloc(sizeof(List));
	assert(p_result!=NULL);
	
	p_result->size = 0;
	p_result->head = NULL;
	p_result->tail = NULL;

	return *p_result;
}
Node* getNewNode(int v){
	Node* res = (Node*)malloc(sizeof(Node));
	assert(res!=NULL);

	res->value = v;
	res->next = NULL;
	res->prev = NULL;

	return res;
}
void insertAtHead(List* p_list, int v){
	Node* p_node = getNewNode(v);

	if(p_list->size < 1)
	{
		p_list->head = p_node;
		p_list->tail = p_node;
	}
	else{
		p_node->next = p_list->head;
		p_list->head->prev = p_node;
		p_list->head = p_node;
	}
	p_list->size += 1;
}
void insertAtTail(List* p_list, int v){
	Node* p_node = getNewNode(v);

	if (p_list->size < 1)
	{
		p_list->tail = p_node;
		p_list->head = p_node;
	}
	else{
		p_list->tail->next = p_node;
		p_node->prev = p_list->tail;
		p_list->tail = p_node;
	}
	p_list->size += 1;
}
int removeAtHead(List* p_list){
	int res;
	
	if (p_list->head == NULL) {
		printf("This list is empty!\n");
		return NULL; //a NULL check should be done when assigning the result of this function
	}
	res = p_list->head->value;
	p_list->head = p_list->head->next;
	p_list->head->prev = NULL;
	p_list->size -= 1;

	return res;
}
int removeAtTail(List* p_list){
	int res;

	if(p_list->tail == NULL){
		printf("This list is empty!\n");
		return NULL; //a NULL check should be done when assigning the result of this function
	}
	res = p_list->tail->value;
	p_list->tail = p_list->tail->prev;
	p_list->tail->next = NULL;
	p_list->size -= 1;

	return res;
}
int searchInList(List* p_list, int v){
	Node* tmp = p_list->head;
	if(tmp == NULL){
		printf("This list is empty!\n");
		return -2;
	}
	int counter = 0;
	while(tmp != NULL)
	{
		if(tmp->value == v)
			break;
		counter++;
		tmp = tmp->next;
	}
	if(counter >= p_list->size)
		counter = -1;

	return counter;
}
void printList(List* p_list){
	Node* tmp = p_list->head;

	while(tmp!=NULL){
		printf("%d\n", tmp->value);
		tmp = tmp->next;
	}
}