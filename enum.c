#include <stdio.h>
#include <stdlib.h>

typedef enum {
	primavera, estate, autunno, inverno
} stagione;

int main(int argc, char const *argv[])
{
	stagione s = autunno;

	switch(s){
		case primavera:
			printf("è primavera\n");
			break;
		case estate:
			printf("è estate\n");
			break;
		case autunno:
			printf("è autunno\n");
			break;
		case inverno:
			printf("è inverno\n");
			break;
		default:
			printf("sei morto.\n");
			break;
	}

	return 0;
}