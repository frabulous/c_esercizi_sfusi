//Esempi sui puntatori

#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	int n;
	int* p;
	int** pp;

	p = &n;
	pp = &p;

	printf("Inserire un intero:\n");
	scanf("%d", p); //cioè &n

	printf("Indirizzo di n: %p == %p == %p\n", *pp, p, &n);

	printf("Valore di n: %d == %d == %d\n",**pp, *p, n);


	char str[11];
	char* a = &str[0];

	printf("Inserire una parola:\n");
	scanf("%s", a);

	printf("terza lettera: %c == %c == %c\n", *(a+2), *(str+2), str[2]);

	for (int i = 0; i < strlen(str); ++i)
	{
		printf("%c", *(str+i));
	}
	printf("\n");

	return 0;
}