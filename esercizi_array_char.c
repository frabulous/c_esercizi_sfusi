/*
ES 1) Leggere 2 array di int lunghi 20 max, contenenti N(=scelta dell'utente) int ciascuno
Creare un altro vettore che contenga solo i numeri pari dei due vettori di partenza e stamparlo

ES 2) Leggere frase (al più 100 char) da tastiera che termina con invio.
- visualizzare la frase
- visualizzare nuova frase costruita aggiungendo dopo ogni vocale minuscola (o maiuscola) la lettera 'f' (o 'F')
*/

#include <stdio.h>
#include <string.h>

#define max_lenght 20
#define max_string 100

int main(int argc, char const *argv[])
{

	//ES 1

	int a1[max_lenght];
	int a2[max_lenght];
	int a3[2*max_lenght];

	int N;

	printf("%s?\n", "Quanti valori si vogliono inserire");
	scanf("%d", &N);

	//int i;
	for(int i=0; i<N; i++){
		printf("%s[%d]:\n", "Inserire il valore di a1", i);
		scanf("%d", &a1[i]);
	}
	for(int i=0; i<N; i++){
		printf("%s[%d]:\n", "Inserire il valore di a2", i);
		scanf("%d", &a2[i]);
	}

	int j=0;
	for(int i=0; i<N; i++){
	
		if (a1[i]%2 == 0){
			a3[j]= a1[i];
			j++;
		}
		if (a2[i]%2 == 0){
			a3[j] = a2[i];
			j++;
		}
	}

	printf("%s:\n", "Array risultato");
	for (int i=0; i < j; ++i)
	{
		printf("a3[%d] = %d\n", i, a3[i]);
	}

	//"pulisco input"
	char c[10];
	fgets(c, 10, stdin); //fflush(stdin);


	//ES 2
	char str[max_string+1]; //1 in più per il carattere di fine stringa

	printf("Inserire una frase (max 100 caratteri):\n");
	fgets(str, sizeof(str), stdin);
	printf("Hai inserito: %s\n", str);

	char res[2*max_string+1];

	j=0;

	for (int i=0; i<strlen(str); ++i)
	{
		printf("i = %d\n", i);
		res[j] = str[i];
		j++;

		if(str[i]=='a' || str[i]=='e' || str[i]=='i' || str[i]=='o' || str[i]=='u')
		{
			res[j] = 'f';
			res[j+1] = str[i];
			j+=2;
		}
		else if(str[i]=='A' || str[i]=='E' || str[i]=='I' || str[i]=='O' || str[i]=='U')
		{
			res[j] = 'F';
			res[j+1] = str[i];
			j+=2;
		}
	}
	res[j] = 0;

	printf("Nuova frase in farfallino:\n %s", res);	

	return 0;
}