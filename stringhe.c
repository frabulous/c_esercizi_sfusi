/*
Stringhe in C
*/

#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	char stringa[50];
	char stringa2[50];

	printf("Inserire stringa (max 50 caratteri):\n");

	scanf("%s", stringa); 
	//N.B: Non serve &stringa perché i nomi degli array corrispondono
	// all'indirizzo del primo elemento, cioè &stringa[0]
	//N.B: Il metodo scanf(%s) ignora i primi white_space, poi legge i 
	// restanti caratteri finché trova \0 o altro white_space
	
	printf("Hai inserito: %s\n", stringa);

	puts("Bravo! Ora inserisci un'altra stringa:\n");
	fgets(stringa2, sizeof(stringa2), stdin);
	//N.B: A differenza di scanf(%s), fgets(s) legge tutta la stringa immessa,
	// inclusi eventuali white_space (anche a inizio stringa)
	//N.B: è altamente sconsigliato l'uso di gets() in quanto non viene specificato
	// il limite del buffer e ciò può comportare diverse situazioni rischiose.
	//N.B: Poiché fgets() viene eseguito dopo scanf(), verrà "saltato" perché il carattere
	// di "a capo" digitato premendo Enter durante scanf() viene registrato in fgets()
	puts(stringa2);

	// Metodi notevoli di string.h:
	
	//// - dimensione di una stringa (restituita in long int -> serve %ld)
	printf("la stringa è lunga %ld\n", strlen(stringa));
	
	//// - confrontare alfabeticamente due stringhe
	printf("stringa rispetto a stringa2: %d\n", strcmp(stringa, stringa2));

	/// - copiare una stringa in un'altra (la seconda nella prima)
	strcpy(stringa, stringa2);
	printf("Nuova stringa: %s\n", stringa);

	//// - concatenare una stringa con un'altra (la seconda alla prima)
	strcat(stringa, stringa2);
	printf("Concatenate: %s\n", stringa);

	return 0;
}