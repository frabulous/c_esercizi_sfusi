#include <stdio.h>
#include <stdlib.h>

int is_correct(int* dd, int* mm, int* yyyy){
	int result = 0;

	if (*yyyy>0 && *yyyy<10000 && *mm>0 && *mm<=12 && *dd>0 && *dd<=31) //bad check
		result = 1;

	return result;
}

int read_date(){
	int sum;
	int date_ok = 0;
	int dd, mm, yyyy;
	
	while(!date_ok)
	{
		printf("Inserire una data (dd/mm/yyyy):\n");
		scanf("%d/%d/%d", &dd, &mm, &yyyy);

		if(!(date_ok = is_correct(&dd, &mm, &yyyy))){
			printf("Formato data errato!\n");
		}
	}

	sum = yyyy*10000 + mm*100 + dd;

	return sum;
}

int compare_dates(int a, int b){
	return a-b;
}

int main(int argc, char const *argv[])
{
	int dd1, mm1, yyyy1;
	int dd2, mm2, yyyy2;

	int sum1 = read_date();
	int sum2 = read_date();

	int res = compare_dates(sum1, sum2);

	//le date coincidono
	if (!res)
		printf("le date coincidono\n");
	//la prima è maggiore della seconda
	else if (res>0)
		printf("la prima piu recente\n");
	//la seconda è maggiore della prima
	else
		printf("la seconda piu recente\n");

	return 0;
}